package com.hakami.zaryadka;

import android.content.Context;
import android.media.MediaPlayer;

public class AudioPlayer
{
	private MediaPlayer mPlayer;
	
	public void play(Context c)
	{
		//stop();
		if( mPlayer != null )
			return;
		
		mPlayer = MediaPlayer.create( c, R.raw.zaryadka_audio );
		
		mPlayer.setOnCompletionListener( new MediaPlayer.OnCompletionListener()
		{
			
			@Override
			public void onCompletion( MediaPlayer mp )
			{
				stop();
			}
		} );
		
		mPlayer.start();
	}
	
	public void stop()
	{
		if( mPlayer != null )
		{
			mPlayer.release();
			mPlayer = null;
		}
	}
}
