package com.hakami.zaryadka;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class AudioZaryadkaFragment extends Fragment
{
	private Button mPlayButton;
	private Button mStopButton;
	private AudioPlayer mPlayer = new AudioPlayer();
	
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState )
	{
		View v = inflater.inflate( R.layout.fragment_audio_zaryadka, parent, false );
		
		mPlayButton = (Button)v.findViewById( R.id.zaryadka_playButton );
		mPlayButton.setOnClickListener( new OnClickListener()
		{

			@Override
			public void onClick( View v )
			{
				mPlayer.play( getActivity() );
			}
			
		});
		mStopButton = (Button)v.findViewById( R.id.zaryadka_stopButton );
		mStopButton.setOnClickListener( new OnClickListener()
		{

			@Override
			public void onClick( View v )
			{
				mPlayer.stop();
			}
			
		});
		
		return v;
	}
	
	@Override 
	public void onDestroy()
	{
		super.onDestroy();
		mPlayer.stop();
	}
}
