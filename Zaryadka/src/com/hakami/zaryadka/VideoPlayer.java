package com.hakami.zaryadka;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlayer
{
	private VideoView mVideo = null;
	private MediaController control = null;
	private static String TAG = "VideoPlayer";
	
	public void play( View v )
	{
		Log.d(TAG, "play is called");
		
		if( mVideo == null )
		{
			
			Log.d(TAG, "In!");
			if(  v.findViewById( R.id.videoView1 ) == null )
				Log.d(TAG, "incorrect view");
			mVideo = (VideoView) v.findViewById( R.id.videoView1 );
			
			if( mVideo == null )
				Log.d(TAG, "mVideo is still null");
			mVideo.setVideoURI( Uri.parse( "android.resource://com.hakami.zaryadka/raw/zaryadka_video"  ) );
		}
		if( control == null )
		{
			control = new MediaController(v.getContext());
			control.setMediaPlayer( mVideo );
			mVideo.setMediaController( control );
		}
		mVideo.start();

	}
	
	public void stop()
	{
		Log.d(TAG, "stop is called");
		
		if( mVideo != null)
		{
			Log.d(TAG, "mVideo is not null");
			mVideo.stopPlayback();
			mVideo = null;
		}
	}

}
