package com.hakami.zaryadka;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class VideoZaryadkaFragment extends Fragment
{
	private static final String TAG = "VideoZaryadkaFragment";
	private VideoPlayer mVideoPlayer = new VideoPlayer();
	private View v;
	private Button mStartButton;
	private Button mStopButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate( savedInstanceState );
		setRetainInstance(true);
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState )
	{
		v = inflater.inflate( R.layout.fragment_video_zaryadka, parent, false );
		
		Log.d(TAG, "Started successfully");
				
		mStartButton = (Button)v.findViewById( R.id.zaryadka_playButton );
		mStartButton.setOnClickListener( new OnClickListener()
		{

			@Override
			public void onClick( View view )
			{
				mVideoPlayer.play( v );
			}
			
		});
		
		mStopButton = (Button)v.findViewById( R.id.zaryadka_stopButton );
		mStopButton.setOnClickListener( new OnClickListener()
		{

			@Override
			public void onClick( View v )
			{
				mVideoPlayer.stop();
			}
			
		});
		
		
		return v;		
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		mVideoPlayer.stop();
	}

}
