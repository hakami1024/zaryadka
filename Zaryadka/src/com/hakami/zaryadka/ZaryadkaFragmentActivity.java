package com.hakami.zaryadka;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class ZaryadkaFragmentActivity extends FragmentActivity
{

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_zaryadka_fragment );
	}

}
